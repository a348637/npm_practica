const log4js = require('log4js');

const logger = log4js.getLogger();
logger.level = "warn";

logger.debug("Iniciando aplicación en modo de pruebas.");
logger.info("La app ha iniciado correctamente");
logger.warn("Falta el archivo config de la app");
logger.error("No se pudo acceder al sistema de archivos");
logger.fatal("Aplicación no se pudo ejecutar en el so");

//supervisor, forever
// mochajs.org
// ESLint

let Hola_lint = 0;

function add(x, y){
    return x + y;
}

module.exports = add;