const add = require('../index');

const assert = require('assert');

describe("Probar la suma de dos números", () => {
    // afirmar que 5 + 5 es 10 
    it("Cinco más cinco es 10", () => {
        assert.equal(10, add(5, 5));
    });

    // afirmamos que cinco más cinco no son 55
    it("Cinco más cinco no son 55", () => {
        assert.notEqual(55, add(5, 5))
    })
});