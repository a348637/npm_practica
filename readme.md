# Práctica | Mi primera aplicación con NPM

Web Plataforms

## Unidad I

Introducción al Desarrollo Web

## Lenguaje de Programación

Javascript

Versión de node: v18.13.0

## Autores

**Juan Antonio Díaz Fernández**

	Usuario: JuanDiazuwu

	Matricula: 348637

* Github de Juan: [Github](https://github.com/JuanDiazuwu)

* Gitlab de Juan: [Gitlab](https://gitlab.com/a348637)

## Instrucciones

Vamos a crear nuestra primera aplicación con node.js utilizando dependencias.

1. Cree un directorio con nombre package_manager.

2. Entre al directorio y ejecute el comando para iniciar una aplicación de node con npm.

3. Instale las siguientes dependencias: Manejador de log's, herramienta de refresco en caliente, pruebas unitarias y lint.

4. Configure su proyecto con git correctamente.

5. Suba la liga de gitlab.

## Manejador de log's

* log4js

## Herramienta de Refresco en Caliente 

* supervisor

## Pruebas Unitarias y Lint

* mocha y eslint